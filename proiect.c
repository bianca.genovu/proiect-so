#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <libgen.h>
#include <sys/stat.h>
#include <sys/wait.h>

int eDirector(const char* path)
{
    struct stat st;
    if(stat(path,&st) == -1)
    {
        return 0;
    }
    return S_ISDIR(st.st_mode);
}

//Functie care imi returneaza timpul ultimei modificari, pentru a o compara mai apoi si a vedea daca ceva s-a modificat intr-un fisier
time_t TimpulUltimeiModificari(const char* path) 
{
    struct stat st;
    if (stat(path, &st) == -1) 
    {
        perror("\nOops, eroare la stat\n");
        exit(-2);
    }
    return st.st_mtime; 
}

//Functie cu ajutorul careia salvez de fiecare data ultimul timp intr-un fisier, pe care il are fiecare director in parte, 
//acest director contine timpul ultimei modificari, salvand totul in fisier imi e posibila compararea pe viitor cu un timp precedent

void SalvareModTime(const char* filename, time_t modTime) 
{
    int fileDescriptor;
    if ((fileDescriptor = open(filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR)) == -1) 
    {
        perror("\nOops, eroare la deschiderea fisierului\n");
        exit(-1);
    }
    
    char buffer[20]; // Alocăm un buffer pentru conversia timpului în șir de caractere
    snprintf(buffer, sizeof(buffer), "%ld", (long)modTime); // Conversie timp_t la șir de caractere

    if (write(fileDescriptor, buffer, strlen(buffer)) == -1) 
    {
        perror("\nOops, eroare la scrierea în fișier\n");
        exit(-1);
    }

    if (close(fileDescriptor) == -1) 
    {
        perror("\nOops, eroare la închiderea fișierului\n");
        exit(-1);
    }
}

//Functia aceasta doar deschide fisierul cu modificarea si ii citeste timpul

time_t PreiaSalvareaPrecedenta(const char* filename) 
{
    int fileDescriptor;
    if ((fileDescriptor = open(filename, O_RDONLY)) == -1) 
    {
        // Dacă nu se poate deschide fișierul, presupunem că nu există o salvare precedentă și returnăm 0
        return 0;
    }

    time_t modTime;
    char buffer[20]; // Alocăm un buffer pentru citirea datelor din fișier

    ssize_t bytes_read = read(fileDescriptor, buffer, sizeof(buffer));
    if (bytes_read == -1) 
    {
        perror("\nOops, eroare la citirea din fișier\n");
        exit(-1);
    } else if (bytes_read == 0) 
    {
        // Dacă nu s-a citit nimic din fișier, presupunem că nu există o salvare precedentă și returnăm 0
        close(fileDescriptor);
        return 0;
    }

    // Convertim șirul de caractere la timp_t
    buffer[bytes_read] = '\0'; // Adăugăm terminatorul de șir
    modTime = atoi(buffer); // Conversie șir la întreg

    if (close(fileDescriptor) == -1) 
    {
        perror("\nOops, eroare la închiderea fișierului\n");
        exit(-1);
    }

    return modTime;
}


///functie pentru cerinta cu modificarile
void parcurgere_folder_recursiv_modificari(const char* director, const char* statusFile)
{
    DIR* dir = opendir(director);
    if (dir == NULL) 
    {
        perror("\nOops, eroare la deschiderea directorului\n");
        exit(-1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) 
        {
            char path[500] = "";
            strcat(path, director);
            strcat(path, "/");
            strcat(path, entry->d_name);

            struct stat file_info;
            if (stat(path, &file_info) < 0) 
            {
                perror("\nOops, eroare la stat\n");
                exit(-2);
            }

            if (S_ISDIR(file_info.st_mode)) 
            {
                // Construiește calea către fișierul de status
                char dirStatusFile[500] = "";
                strcat(dirStatusFile, path);
                strcat(dirStatusFile, "/");
                strcat(dirStatusFile, statusFile);

                // Parcurgerea recursivă
                parcurgere_folder_recursiv_modificari(path, statusFile);

                // Verifică timpul ultimei modificări și îl compară cu cel salvat
                time_t inainte = PreiaSalvareaPrecedenta(dirStatusFile);
                time_t acum = TimpulUltimeiModificari(path);

                if (inainte != acum)
                {
                    printf("Directorul '%s' a fost modificat\n", path);
                    SalvareModTime(dirStatusFile, acum);
                } 
                else 
                {
                    printf("Nu s-a efectuat nicio modificare în directorul '%s'\n", path);
                }
            } 
            else
            {
                // Construiește calea către fișierul de status
                char fileStatusFile[500] = "";
                char* ultimulSlash = strrchr(path,'/'); 
                char* numeFile = ultimulSlash + 1;
                if(strcmp(numeFile,statusFile) != 0)
                {
                    strcat(fileStatusFile,numeFile);
                    strcat(fileStatusFile,statusFile);
                
                    // Verifică timpul ultimei modificări și îl compară cu cel salvat
                    time_t inainte = PreiaSalvareaPrecedenta(fileStatusFile);
                    time_t acum = TimpulUltimeiModificari(path);

                    if (inainte != acum)
                    {
                        printf("Fisierul '%s' a fost modificat\n", path);
                        SalvareModTime(fileStatusFile, acum);
                    } 
                    else 
                    {
                        printf("Nu s-a efectuat nicio modificare în fisierul '%s'\n", path);
                    }   
                }
                
            }
        }
    }

    closedir(dir);
}


// Functie pentru a muta fișierele fără permisiuni într-un director țintă
void muta_fisier(const char* sursa, const char* destinatie) 
{
    if (rename(sursa, destinatie) != 0) {
        perror("Eroare la mutarea fișierului");
    }
}

void parcurgere_fisier_recursiv(const char* director, const char* script_path, const char* director_tinta,int count) 
{
    DIR* dir = opendir(director);
    if (dir == NULL) 
    {
        perror("\nOops, eroare la deschiderea directorului\n");
        exit(-1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) 
        {
            char path[500] = "";
            strcat(path, director);
            strcat(path, "/");
            strcat(path, entry->d_name);

            struct stat file_info;
            if (stat(path, &file_info) < 0) 
            {
                perror("\nOops, eroare la stat\n");
                exit(-2);
            }

            if (S_ISDIR(file_info.st_mode)) 
            {
                parcurgere_fisier_recursiv(path, script_path, director_tinta,count+1); // Recursivitate

            } else if (S_ISREG(file_info.st_mode))//daca e fisier 
            {
                if ((file_info.st_mode & S_IRWXU) == 0 && (file_info.st_mode & S_IRWXG) == 0 && (file_info.st_mode & S_IRWXO) == 0) 
                { //fara permisiuni
                    //verifica daca fisierul contine pattern-ul dorit
                        int pid;
                        int pfd[2];
                        pid_t wpid;
                        char output[100];
                        if(pipe(pfd) < 0)
                        {
                            perror("\nOops, eroare la pipe\n");
                            exit(-10);
                        }
                        if((pid = fork()) < 0)
                        {
                            perror("\nOops, eroare la fork\n");
                            exit(-11);
                        }
                        if(pid == 0)
                        {
                            close(pfd[0]);
                            dup2(pfd[1],1);
                            if(execlp("./script.sh","script.sh",script_path,NULL) == -1)
                            {
                                perror("\nOops, eroare la script\n");
                                exit(-12);
                            }
                        }
                        else
                        {
                            close(pfd[1]);
                            int status;
                            wpid = wait(&status);
                            if (WIFEXITED(status)) 
                            {
                                printf("\nPid-ul procesului pentru mutare %d a ieșit cu codul: %d\n", wpid, WEXITSTATUS(status));
                            } 
                            else 
                            {
                                fprintf(stderr, "Eroare la așteptarea procesului\n");
                            }
                            read(pfd[0],output,2*sizeof(char));
                            close(pfd[0]);
                            if(output[0] == 'D')
                            {
                                printf("Fișierul '%s' conține pattern-ul. Va fi mutat.\n", path);
                                //construiește calea pentru fișierul mutat
                                char cale_destinatie[1024];
                                snprintf(cale_destinatie, sizeof(cale_destinatie), "%s/%s", director_tinta, entry->d_name);
                                //muta fișierul în directorul țintă
                                muta_fisier(path, cale_destinatie);
                            }
                        }
                        
                    }
            }
        }
    }

}

int main(int argc, char* argv[]) 
{
    if (argc < 4) 
    { // Trebuie să ai cel puțin 4 argumente: program, script, director_tinta, restu
        fprintf(stderr, "\nOops, trebuie să fie cel puțin 4 argumente (program, script, director_tinta, restu)\n");
        exit(-4);
    }

    pid_t pid[argc], wpid;
    int status;
    const char* statusFile = "modificare_precedenta.txt";//numele fisierului une scriu ultimele modificari

    for (int i = 3; i < argc; i++) 
    { 
        if (eDirector(argv[i])) 
        {
            if ((pid[i] = fork()) == -1) 
            {
                perror("\nOops, eroare la fork\n");
            }

            if (pid[i] == 0) 
            { 
                parcurgere_folder_recursiv_modificari(argv[i], statusFile);
                exit(0); 
            }
        }
    }

    for (int i = 3; i < argc; i++) 
    { 
            wpid = wait(&status);
            if (WIFEXITED(status)) 
            {
                printf("\nPid-ul procesului pentru ultima modificare %d a ieșit cu codul: %d\n", wpid, WEXITSTATUS(status));
            } 
            else 
            {
                fprintf(stderr, "Eroare la așteptarea procesului\n");
            }
    }

    const char* script_path = argv[1];
    const char* director_tinta = argv[2]; // Directorul unde voi muta fișierele fără permisiuni și cu pattern-ul dorit

    for (int i = 3; i < argc; i++) 
    { 
        if (eDirector(argv[i]))
        {
            parcurgere_fisier_recursiv(argv[i], script_path, director_tinta,0);
        }
    }

    return 0;
}