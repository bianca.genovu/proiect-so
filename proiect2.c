#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <libgen.h>
#include <sys/stat.h>
//Functie care imi returneaza timpul ultimei modificari, pe a o compara mai apoi si a vedea daca ceva s-a modificat intr-un fisier
time_t TimpulUltimeiModificari(const char* path) {
    struct stat st;
    if (stat(path, &st) == -1) {
        perror("\nOops, eroare la stat\n");
        exit(-2);
    }
    return st.st_mtime; 
}
//Functie cu ajutorul careia salvez de fiecare data ultimul timp intr-un fisier, pe care il are fiecare director in parte, 
//acest director contine timpul ultimei modificari, salvand totul in fisier imi e posibila compararea pe viitor cu un timp precedent
void SalvareModTime(const char* filename, time_t modTime) {
    FILE *f;
    if ((f = fopen(filename, "w")) == NULL) {
        perror("\nOops, eroare la deschiderea fisierului\n");
        exit(-1);
    }
    fprintf(f, "%ld", (long)modTime);
    if(fclose(f) != 0)
    {
        perror("\nOops, eroare la inchiderea fisierului\n");
        exit(-1);
    }
}
//
time_t PreiaSalvareaPrecedenta(const char* filename) {
    FILE *f;
    struct stat st;
    if ((f = fopen(filename, "r")) == NULL){
        return 0; // Daca folder-ul meu nu este creat deja in directorul respectiv voi citi timpul ca fiind
        //cel din momentul ultimei modificari al acestuia
    }
    time_t modTime;
    fscanf(f, "%ld", &modTime);
    if(fclose(f) != 0)
    {
        perror("\nOops, eroare la inchiderea fisierului\n");
        exit(-1);
    }
    return modTime;//citesc timpul scris in fisier si il returnez pentru a-l compara cu timpul actual
}

void parcurgere_fisier_recursiv(const char* director, const char* statusFile,int count)
{
    DIR* dir = opendir(director);
    if(dir == NULL)
    {
        perror("\nOops, eroare la deschiderea directorului\n");
        exit(-1);
    }
    /*
    if(count == 0)
    {
        if(chdir(director) == -1)
        {
            perror("Oops, eroare aici");
            exit(-5);
        }

        time_t inainte = PreiaSalvareaPrecedenta(director);
        time_t acum = TimpulUltimeiModificari(director);
        if (inainte != acum) {
            printf("Directorul '%s' a fost modificat\n", director);
            SalvareModTime(director, acum);
        } else {
            printf("Nu s-a efectuat nicio modificare in directorul '%s'\n", director);
        }
            count++;
        }
    */
     
    struct dirent *entry;
    while((entry = readdir(dir)) != NULL)
    {
        if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
        {
            char path[500] = "";
            strcat(path, director);
            strcat(path, "/");
            strcat(path, entry->d_name);
            struct stat file_info;
            if(stat(path, &file_info) < 0)
            {
                perror("\nOops, eroare la stat\n");
                exit(-2);
            }
           
            if(S_ISDIR(file_info.st_mode) == 1) //daca e 1 a gasit fisier nou si cauta sa vada daca se poate duce in continuare si verific daca a aparut vreo modificare
            {   
                char dirStatusFile[500] = "";
                strcat(dirStatusFile, path);
                strcat(dirStatusFile, "/");
                strcat(dirStatusFile, statusFile);
                //timpul scris in fisier
                time_t inainte = PreiaSalvareaPrecedenta(dirStatusFile);
                //parcurgerea recursiva, in cazul in care mai am alte directoare in directorul meu actual
                parcurgere_fisier_recursiv(path, statusFile,count);
                //timpul actual al ultimei modificari
                time_t acum = TimpulUltimeiModificari(path);
                //daca timpii sunt aceiasi fisierul nu a avut parte de modificari
                //daca timpii sunt diferiti fisierul a avut parte de modificari
                if (inainte != acum) {
                    printf("Directorul '%s' a fost modificat\n", path);
                    SalvareModTime(dirStatusFile, acum);
                } else {
                    printf("Nu s-a efectuat nicio modificare in directorul '%s'\n", path);
                }
            }


        }
    }

    closedir(dir);
}
//Mica nota, daca adaug un folder nou in folder, folder-ul nou adaugat va figura ca neavand nicio modificare, deoarece
//in functia PreiaSalvareaPrecedenta returnez timpul ultimei modificari, nu stiu exact cum se doreste urmarirea folder-ului
//nou creat, daca vreti sa figureze si folder-ul nou ca modificat in loc de return st.st_mtime pun return 0, in situatia asta
//folderul imi apare modificat pt 2 iteratii, nu inteleg momentan de ce
int main(int argc, char* argv[]) {
    if (argc < 2) {
        perror("\nOops, eroare la numarul de argumente in linia de comanda nu coincide cu cel cerut de problema\n");
        exit(-4);
    }

    const char* statusFile = "modificare_precedenta.txt";

    for(int i = 1; i < argc; i++)
    {
        parcurgere_fisier_recursiv(argv[i], statusFile,0);
    }
    /*
    for(int i = 1; i < argc; i++)
    {
        printf("%d\n",i);
        time_t inainte = PreiaSalvareaPrecedenta(statusFile);
        printf("%ld\n",inainte);
        time_t acum = TimpulUltimeiModificari(argv[i]);
        printf("%ld\n",acum);
        if (inainte != acum)
        {
            printf("Directorulul '%s' a fost modificat\n",argv[i]);
            SalvareModTime(argv[i], acum);
        } else 
        {
            printf("Nu s-a efectuat nicio modificare in directorul '%s'\n", argv[i]);
        }
    }
    */
    

    return 0;
}

