#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <libgen.h>
#include <sys/stat.h>
#include <sys/wait.h>

int eDirector(const char* path)
{
    struct stat st;
    if(stat(path,&st) == -1)
    {
        return 0;
    }

    return S_ISDIR(st.st_mode);
}
//Fork != -1
//Functie care imi returneaza timpul ultimei modificari, pe a o compara mai apoi si a vedea daca ceva s-a modificat intr-un fisier
time_t TimpulUltimeiModificari(const char* path) {
    struct stat st;
    if (stat(path, &st) == -1) {
        perror("\nOops, eroare la stat\n");
        exit(-2);
    }
    return st.st_mtime; 
}
//Functie cu ajutorul careia salvez de fiecare data ultimul timp intr-un fisier, pe care il are fiecare director in parte, 
//acest director contine timpul ultimei modificari, salvand totul in fisier imi e posibila compararea pe viitor cu un timp precedent
void SalvareModTime(const char* filename, time_t modTime) {
    FILE *f;
    if ((f = fopen(filename, "w")) == NULL) {
        perror("\nOops, eroare la deschiderea fisierului\n");
        exit(-1);
    }
    fprintf(f, "%ld", (long)modTime);
    if(fclose(f) != 0)
    {
        perror("\nOops, eroare la inchiderea fisierului\n");
        exit(-1);
    }
}
//
time_t PreiaSalvareaPrecedenta(const char* filename) {
    FILE *f;
    if ((f = fopen(filename, "r")) == NULL){
        return 0; // Daca folder-ul meu nu este creat deja in directorul respectiv voi citi timpul ca fiind
        //cel din momentul ultimei modificari al acestuia
    }
    time_t modTime;
    fscanf(f, "%ld", &modTime);
    if(fclose(f) != 0)
    {
        perror("\nOops, eroare la inchiderea fisierului\n");
        exit(-1);
    }
    return modTime;//citesc timpul scris in fisier si il returnez pentru a-l compara cu timpul actual
}

///functie pentru cerinta cu modificarile
void parcurgere_folder_recursiv_modificari(const char* director, const char* statusFile)
{
    DIR* dir = opendir(director);
    if (dir == NULL) 
    {
        perror("\nOops, eroare la deschiderea directorului\n");
        exit(-1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) 
        {
            char path[500] = "";
            strcat(path, director);
            strcat(path, "/");
            strcat(path, entry->d_name);

            struct stat file_info;
            if (stat(path, &file_info) < 0) 
            {
                perror("\nOops, eroare la stat\n");
                exit(-2);
            }

            if (S_ISDIR(file_info.st_mode)) 
            {
                // Construiește calea către fișierul de status
                char dirStatusFile[500] = "";
                strcat(dirStatusFile, path);
                strcat(dirStatusFile, "/");
                strcat(dirStatusFile, statusFile);

                // Parcurgerea recursivă
                parcurgere_folder_recursiv_modificari(path, statusFile);

                // Verifică timpul ultimei modificări și îl compară cu cel salvat
                time_t inainte = PreiaSalvareaPrecedenta(dirStatusFile);
                time_t acum = TimpulUltimeiModificari(path);

                if (inainte != acum)
                {
                    printf("Directorul '%s' a fost modificat\n", path);
                    SalvareModTime(dirStatusFile, acum);
                } 
                else 
                {
                    printf("Nu s-a efectuat nicio modificare în directorul '%s'\n", path);
                }
            } 
        }
    }

    closedir(dir);
}

void muta_fisier(const char* sursa, const char* destinatie) 
{        
    if (rename(sursa, destinatie) != 0) {
        perror("Eroare la mutarea fișierului");
    }
}

// Funcție pentru parcurgerea recursivă a directorului
void parcurgere_folder_recursiv(const char* director, const char* script_path, const char* director_tinta,int count) 
{
    DIR* dir = opendir(director);
    if (dir == NULL) {
        perror("Eroare la deschiderea directorului");
        exit(-1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
        {
            char path[500] = "";
            strcat(path, director);
            strcat(path, "/");
            strcat(path, entry->d_name);

            struct stat file_info;
            if (stat(path, &file_info) < 0)
            {
                perror("Eroare la stat");
                exit(-2);
            }

            if (S_ISDIR(file_info.st_mode))
            {
                // Parcurgere recursivă pentru directoare
                parcurgere_folder_recursiv(path, script_path, director_tinta,count+1);

            } 
            else if (S_ISREG(file_info.st_mode)) 
            {
                ///verificare de permisiuni
                if ((file_info.st_mode & S_IRWXU) == 0 && (file_info.st_mode & S_IRWXG) == 0 && (file_info.st_mode & S_IRWXO) == 0) 
                {
                    char command[1024];
                    snprintf(command, sizeof(command), "bash %s %s", script_path, path); 
                    pid_t pid[100];
                    int fd[2];
                    pipe(fd);
                    if ((pid[count] = fork()) == -1) 
                    {
                        perror("\nOops, eroare la fork\n");
                    }
                    if (pid[count] == 0) 
                    { 
                        close(fd[0]);
                        dup2(fd[1],1);
                        system(command); 
                        //1 daca e pattern-ul, 0 daca nu e 
                        exit(1); 
                    }
                    else
                    {
                        close(fd[1]);
                        char cale_destinatie[1024];
                        snprintf(cale_destinatie, sizeof(cale_destinatie), "%s/%s", director_tinta, entry->d_name);
                        char citit[500];
                        read(fd[0],citit,500);
                        if(strcmp(citit,"DA") == 0)
                        {
                            muta_fisier(path, cale_destinatie);
                        }
                        close(fd[0]);
                        exit(0); 
                    }
                }
            }
        }
    }
    closedir(dir);
}
// CERINTA
int main(int argc, char* argv[]) 
{
    if (argc < 4) 
    { // Trebuie să ai cel puțin 4 argumente: numele programului, calea către script, directorul unde mut și cel puțin un director in care caut
        fprintf(stderr, "\nOops, trebuie să fie cel puțin 3 argumente (program, script, director)\n");
        exit(-4);
    }

    const char* script_path = argv[1]; // Calea către scriptul shell
    const char* statusFile = "modificare_precedenta.txt";//numele fisierului une scriu ultimele modificari

    pid_t pid[argc], wpid;
    int status;

    for (int i = 3; i < argc; i++) 
    { 
        if (eDirector(argv[i])) 
        {
            if ((pid[i] = fork()) == -1) 
            {
                perror("\nOops, eroare la fork\n");
            }

            if (pid[i] == 0) 
            { 
                parcurgere_folder_recursiv_modificari(argv[i], statusFile);
                exit(0); 
            }
        }
    }

    for (int i = 3; i < argc; i++) 
    { 
            wpid = wait(&status);
            if (WIFEXITED(status)) 
            {
                printf("\nPid-ul procesului pentru ultima modificare %d a ieșit cu codul: %d\n", wpid, WEXITSTATUS(status));
            } 
            else 
            {
                fprintf(stderr, "Eroare la așteptarea procesului\n");
            }
    }
    const char* director_tinta = argv[2]; // Directorul unde vei muta fișierele fără permisiuni și cu pattern-ul dorit

     for (int i = 3; i < argc; i++) 
    { 
        if (eDirector(argv[i])) 
        {
            if ((pid[i] = fork()) == -1) 
            {
                perror("\nOops, eroare la fork\n");
            }

            if (pid[i] == 0) 
            { 
                parcurgere_folder_recursiv(argv[i], script_path,director_tinta,0);
                exit(0); 
            }
        }
    }

    for (int i = 3; i < argc; i++) 
    { 
            wpid = wait(&status);
            if (WIFEXITED(status)) 
            {
                printf("\nPid-ul procesului pentru ultima modificare %d a ieșit cu codul: %d\n", wpid, WEXITSTATUS(status));
            } 
            else 
            {
                fprintf(stderr, "Eroare la așteptarea procesului\n");
            }
    }
    
    return 0;
}


