#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <libgen.h>
#include <sys/stat.h>
#include <sys/wait.h>

int eDirector(const char* path)
{
    struct stat st;
    if(stat(path,&st) == -1)
    {
        return 0;
    }
    return S_ISDIR(st.st_mode);
}

//Functie care imi returneaza timpul ultimei modificari, pe a o compara mai apoi si a vedea daca ceva s-a modificat intr-un fisier
time_t TimpulUltimeiModificari(const char* path) {
    struct stat st;
    if (stat(path, &st) == -1) {
        perror("\nOops, eroare la stat\n");
        exit(-2);
    }
    return st.st_mtime; 
}

//Functie cu ajutorul careia salvez de fiecare data ultimul timp intr-un fisier, pe care il are fiecare director in parte, 
//acest director contine timpul ultimei modificari, salvand totul in fisier imi e posibila compararea pe viitor cu un timp precedent

void SalvareModTime(const char* filename, time_t modTime) {
    FILE *f;
    if ((f = fopen(filename, "w")) == NULL) {
        perror("\nOops, eroare la deschiderea fisierului\n");
        exit(-1);
    }
    fprintf(f, "%ld", (long)modTime);
    if(fclose(f) != 0)
    {
        perror("\nOops, eroare la inchiderea fisierului\n");
        exit(-1);
    }
}
//
time_t PreiaSalvareaPrecedenta(const char* filename) {
    FILE *f;
    if ((f = fopen(filename, "r")) == NULL){
        return 0; // Daca folder-ul meu nu este creat deja in directorul respectiv voi citi timpul ca fiind
        //cel din momentul ultimei modificari al acestuia
    }
    time_t modTime;
    fscanf(f, "%ld", &modTime);
    if(fclose(f) != 0)
    {
        perror("\nOops, eroare la inchiderea fisierului\n");
        exit(-1);
    }
    return modTime;//citesc timpul scris in fisier si il returnez pentru a-l compara cu timpul actual
}

///functie pentru cerinta cu modificarile
void parcurgere_folder_recursiv_modificari(const char* director, const char* statusFile)
{
    DIR* dir = opendir(director);
    if (dir == NULL) 
    {
        perror("\nOops, eroare la deschiderea directorului\n");
        exit(-1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) 
        {
            char path[500] = "";
            strcat(path, director);
            strcat(path, "/");
            strcat(path, entry->d_name);

            struct stat file_info;
            if (stat(path, &file_info) < 0) 
            {
                perror("\nOops, eroare la stat\n");
                exit(-2);
            }

            if (S_ISDIR(file_info.st_mode)) 
            {
                // Construiește calea către fișierul de status
                char dirStatusFile[500] = "";
                strcat(dirStatusFile, path);
                strcat(dirStatusFile, "/");
                strcat(dirStatusFile, statusFile);

                // Parcurgerea recursivă
                parcurgere_folder_recursiv_modificari(path, statusFile);

                // Verifică timpul ultimei modificări și îl compară cu cel salvat
                time_t inainte = PreiaSalvareaPrecedenta(dirStatusFile);
                time_t acum = TimpulUltimeiModificari(path);

                if (inainte != acum)
                {
                    printf("Directorul '%s' a fost modificat\n", path);
                    SalvareModTime(dirStatusFile, acum);
                } 
                else 
                {
                    printf("Nu s-a efectuat nicio modificare în directorul '%s'\n", path);
                }
            } 
        }
    }

    closedir(dir);
}


// Functie pentru a muta fișierele fără permisiuni într-un director țintă
void muta_fisier(const char* sursa, const char* destinatie) {
    if (rename(sursa, destinatie) != 0) {
        perror("Eroare la mutarea fișierului");
    }
}

void parcurgere_fisier_recursiv(const char* director, const char* script_path, const char* director_tinta,int count) {
    DIR* dir = opendir(director);
    if (dir == NULL) {
        perror("\nOops, eroare la deschiderea directorului\n");
        exit(-1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char path[500] = "";
            strcat(path, director);
            strcat(path, "/");
            strcat(path, entry->d_name);

            struct stat file_info;
            if (stat(path, &file_info) < 0) 
            {
                perror("\nOops, eroare la stat\n");
                exit(-2);
            }

            if (S_ISDIR(file_info.st_mode)) 
            {
                parcurgere_fisier_recursiv(path, script_path, director_tinta,count+1); // Recursivitate

            } else if (S_ISREG(file_info.st_mode))//daca e fisier 
            {
                if ((file_info.st_mode & S_IRWXU) == 0 && (file_info.st_mode & S_IRWXG) == 0 && (file_info.st_mode & S_IRWXO) == 0) 
                { //fara permisiuni
                    //verifica daca fisierul contine pattern-ul dorit
                        int pid;
                        int pfd[2];
                        pid_t wpid;
                        char output[100];
                        if(pipe(pfd) < 0)
                        {
                            perror("\nOops, eroare la pipe\n");
                            exit(-10);
                        }
                        if((pid = fork()) < 0)
                        {
                            perror("\nOops, eroare la fork\n");
                            exit(-11);
                        }
                        if(pid == 0)
                        {
                            close(pfd[0]);
                            dup2(pfd[1],1);
                            if(execlp("./script.sh","script.sh",script_path,NULL) == -1)
                            {
                                perror("\nOops, eroare la script\n");
                                exit(-12);
                            }
                        }
                        else
                        {
                            close(pfd[1]);
                            int status;
                            wpid = wait(&status);
                            if (WIFEXITED(status)) 
                            {
                                printf("\nPid-ul procesului pentru mutare %d a ieșit cu codul: %d\n", wpid, WEXITSTATUS(status));
                            } 
                            else 
                            {
                                fprintf(stderr, "Eroare la așteptarea procesului\n");
                            }
                            read(pfd[0],output,2*sizeof(char));
                            close(pfd[0]);
                            if(output[0] == 'D')
                            {
                                printf("Fișierul '%s' conține pattern-ul. Va fi mutat.\n", path);
                                //construiește calea pentru fișierul mutat
                                char cale_destinatie[1024];
                                snprintf(cale_destinatie, sizeof(cale_destinatie), "%s/%s", director_tinta, entry->d_name);
                                //muta fișierul în directorul țintă
                                muta_fisier(path, cale_destinatie);
                            }
                        }
                        
                    }
            }
        }
    }

}

int main(int argc, char* argv[]) 
{
    if (argc < 4) 
    { // Trebuie să ai cel puțin 4 argumente: script, director, director_tinta
        fprintf(stderr, "\nOops, trebuie să fie cel puțin 4 argumente (program, script, director, director_tinta)\n");
        exit(-4);
    }

    pid_t pid[argc], wpid;
    int status;
    const char* statusFile = "modificare_precedenta.txt";//numele fisierului une scriu ultimele modificari

    for (int i = 3; i < argc; i++) 
    { 
        if (eDirector(argv[i])) 
        {
            if ((pid[i] = fork()) == -1) 
            {
                perror("\nOops, eroare la fork\n");
            }

            if (pid[i] == 0) 
            { 
                parcurgere_folder_recursiv_modificari(argv[i], statusFile);
                exit(0); 
            }
        }
    }

    for (int i = 3; i < argc; i++) 
    { 
            wpid = wait(&status);
            if (WIFEXITED(status)) 
            {
                printf("\nPid-ul procesului pentru ultima modificare %d a ieșit cu codul: %d\n", wpid, WEXITSTATUS(status));
            } 
            else 
            {
                fprintf(stderr, "Eroare la așteptarea procesului\n");
            }
    }

    const char* script_path = argv[1];
    const char* director_tinta = argv[2]; // Directorul unde voi muta fișierele fără permisiuni și cu pattern-ul dorit

    for (int i = 3; i < argc; i++) 
    { 
        if (eDirector(argv[i]))
        {
            parcurgere_fisier_recursiv(argv[i], script_path, director_tinta,0);
        }
    }

    return 0;
}